# kubevm

## Get kubespray:

Actual kubespray suports max python 3.10

### What to do:
- checkout submodules (kubespray)
- if not present, install pyenv 
- if not present, install python 3.10.11
- create venv with python 3.10
- install requirements-2.12.txt placed in kubespray


```bash
git submodule update
brew install pyenv
pyenv install -v 3.10.11
/Users/admin/.pyenv/versions/3.10.11/bin/python -m venv .venv
source .venv/bin/activate
pip install -r kubevm/ansible/kubespray/requirements-2.12.txt
```

## direnv
To easier maitain local enviroments you can use dienv

```bash
brew install direnv
echo 'eval "$(direnv hook bash)"' >> ~/.bashrc
echo 'eval "$(direnv hook zsh)"' >> ~/.zshrc
```

Then you can create .envrc file with needed variables.
When you will go into diretory, variables will be present.

Example:
```bash
export PROXMOX_VE_USERNAME=root@pam
export PROXMOX_VE_PASSWORD=<password>
```