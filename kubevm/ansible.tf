resource "local_file" "ansible_inventory" {
  filename = "./ansible/inventory.ini"
  content     = <<-EOF
%{ for cp in proxmox_virtual_environment_vm.k8s_cps ~}
  ${cp.name} ansible_host=${cp.ipv4_addresses[1][0]} ansible_become=true ansible_user=przemek
%{ endfor ~}
%{ for worker in proxmox_virtual_environment_vm.k8s_workers ~}
  ${worker.name} ansible_host=${worker.ipv4_addresses[1][0]} ansible_become=true ansible_user=przemek
%{ endfor ~}

  [kube_control_plane]
%{ for cp in proxmox_virtual_environment_vm.k8s_cps ~}
  ${cp.name}
%{ endfor ~}
  
  [etcd]
%{ for cp in proxmox_virtual_environment_vm.k8s_cps ~}
  ${cp.name}
%{ endfor ~}
  
  [kube_node]
%{ for worker in proxmox_virtual_environment_vm.k8s_workers ~}
  ${worker.name}
%{ endfor ~}
  
  [k8s_cluster:children]
  kube_node
  kube_control_plane
  
  EOF

  provisioner "local-exec" {
    command = "ansible-playbook -vvvvv -i inventory.ini playbook.yml"
    working_dir = "ansible"
    environment = {
      ANSIBLE_HOST_KEY_CHECKING = "false"
    }
  }
}