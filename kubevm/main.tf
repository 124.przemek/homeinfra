terraform {
  required_providers {
    proxmox = {
      source  = "bpg/proxmox"
      version = "0.8.0"
    }
  }
}

provider "proxmox" {
  virtual_environment {
    endpoint = "https://pve.tomala.dev/"
  }
}