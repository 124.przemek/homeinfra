resource "proxmox_virtual_environment_vm" "k8s_cps" {
  count = 3
  name        = "k8s-cp-0${count.index + 1}"
  description = "Managed by Terraform"
  tags        = ["terraform"]
  node_name   = "pve${count.index + 1}"

  cpu {
    cores = 2
  }

  memory {
    dedicated = 6144
  }

  agent {
    enabled = true
  }

  network_device {
    bridge = "vmbr0"
  }

  disk {
    datastore_id = "local-lvm"
    file_id      = proxmox_virtual_environment_file.debian_cloud_image.id
    interface    = "scsi0"
    size         = 32
  }

  serial_device {} # The Debian cloud image expects a serial port to be present

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }

  initialization {
    datastore_id      = "local-lvm"
    user_data_file_id = proxmox_virtual_environment_file.cloud_config.id
    ip_config {
      ipv4 {
        address = "192.168.17.3${count.index + 1}/24"
        # address = cidrsubnet("192.168.17.0/24", 8, count.index + 31) # firt one 192.168.17.31
        gateway = "192.168.17.1"
      }
    }
  }
}

resource "proxmox_virtual_environment_vm" "k8s_workers" {
  count = 3
  name        = "k8s-worker-0${count.index + 1}"
  description = "Managed by Terraform"
  tags        = ["terraform"]
  node_name   = "pve${count.index + 1}"

  cpu {
    cores = 1
  }

  memory {
    dedicated = 2048
  }

  agent {
    enabled = true
  }

  network_device {
    bridge = "vmbr0"
  }

  disk {
    datastore_id = "local-lvm"
    file_id      = proxmox_virtual_environment_file.debian_cloud_image.id
    interface    = "scsi0"
    size         = 32
  }

  serial_device {} # The Debian cloud image expects a serial port to be present

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }

  initialization {
    datastore_id      = "local-lvm"
    user_data_file_id = proxmox_virtual_environment_file.cloud_config.id
    ip_config {
      ipv4 {
        address = "192.168.17.4${count.index + 1}/24"
        # address = cidrsubnet("192.168.17.0/24", 8, count.index + 41) # first one 192.168.17.41
        gateway = "192.168.17.1"
      }
    }
  }
}